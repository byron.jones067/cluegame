package com.byjones.cluegame;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.io.FileReader;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Board extends JPanel {

	private static final long serialVersionUID = 1L;
	private int numRows;
	private int numColumns;
	@SuppressWarnings("unused")
	private static final int MAX_BOARD_SIZE = 50;
	private BoardCell[][] board;
	private Map<Character, String> legend;
	private Map<BoardCell, Set<BoardCell>> adjMatrix = new HashMap<BoardCell, Set<BoardCell>>();
	private Set<BoardCell> targets;
	private Set<BoardCell> visited;
	private String boardConfigFile;
	private String roomConfigFile;
	private String playerConfigFile;
	private String weaponConfigFile;
	private ArrayList<Card> deck;
	private ArrayList<Card> playerCards;
	private ArrayList<Card> weaponCards;
	private ArrayList<ComputerPlayer> cpuPlayers;
	private ArrayList<Player> players;
	private HumanPlayer humanPlayer;
	private Solution solution;
	private int roll = 6;
	private int currentPlayerIndex = 0;
	private Random rand = new Random();
	private Card response = null;
	private Solution suggestion = null;
	private boolean wasDisproved = true;
	// variable used for singleton pattern
	private static Board theInstance = new Board();

	// ctor is private to ensure only one can be created
	private Board() {
	}

	// this method returns the only Board
	public static Board getInstance() {
		return theInstance;
	}

	public void setConfigFiles(String boardCF, String roomCF, String playerCF, String weaponCF) {
		boardConfigFile = boardCF;
		roomConfigFile = roomCF;
		playerConfigFile = playerCF;
		weaponConfigFile = weaponCF;
	}

	// Reads legend file into Map instance
	public void loadRoomConfig() throws FileNotFoundException, BadConfigFormatException {

		if (deck == null) {
			deck = new ArrayList<Card>();
		}
		legend = new HashMap<Character, String>();
		FileReader reader = new FileReader(roomConfigFile);
		Scanner in = new Scanner(reader);

		while (in.hasNextLine()) {
			String str = in.nextLine();

			if (str.substring(str.lastIndexOf(",") + 2).equals("Card")) {
				deck.add(new Card(str.substring(str.indexOf(",") + 1, str.lastIndexOf(",")).trim(), CardType.ROOM));

			} else if (!str.substring(str.lastIndexOf(",") + 2).equals("Other")) {
				in.close();
				throw new BadConfigFormatException("Incorrect Room Type: room should be of type 'Card' or 'Other'");

			}

			legend.put(str.charAt(0), str.substring(3, str.indexOf(",", 3)));

		}
		in.close();
	}

	// Reads board map into the board 2D array
	public void loadBoardConfig() throws FileNotFoundException, BadConfigFormatException {

		FileReader reader = new FileReader(boardConfigFile);
		Scanner in = new Scanner(reader);
		numRows = 0;
		HashMap<Integer, String[]> boardIns = new HashMap<Integer, String[]>();

		while (in.hasNextLine()) {
			String holder = in.nextLine();
			String[] split = holder.split(",");
			// String strHold = "";
			boardIns.put(numRows, split);
			numRows++;
		}

		numColumns = boardIns.get(0).length;
		board = new BoardCell[numRows][numColumns];
		for (int row = 0; row < numRows; ++row) {
			for (int col = 0; col < numColumns; ++col) {

				if (boardIns.get(row).length != numColumns) {
					in.close();
					throw new BadConfigFormatException(
							"Incorrect number of columns: number of columns should be " + numColumns);
				}

				String initHold = boardIns.get(row)[col];
				if (initHold.length() > 1) {
					board[row][col] = new BoardCell(row, col, initHold.charAt(0), initHold.charAt(1));
				} else {
					if (!legend.containsKey(initHold.charAt(0))) {
						in.close();
						throw new BadConfigFormatException("Incorrect room initial: this room is not in the legend");
					}
					board[row][col] = new BoardCell(row, col, initHold.charAt(0));
				}
			}
		}
		in.close();

	}

	public void initialize() {
		humanPlayer = null;
		legend = new HashMap<Character, String>();
		targets = new HashSet<BoardCell>();
		deck = new ArrayList<Card>();
		players = new ArrayList<Player>();
		cpuPlayers = new ArrayList<ComputerPlayer>();
		currentPlayerIndex = 0;
		try {
			loadConfigFiles();
		} catch (FileNotFoundException e) {
			System.out.println("Error loading config file " + e);
		} catch (BadConfigFormatException e) {
			System.out.println("There was a config error");
			System.out.println(e);
			System.out.println(e.getMessage());
		}
		// required to pass old tests
		if (this.playerConfigFile != null) {
			this.dealDeck();
		}
		calcAdjacencies();

		this.addMouseListener(new BoardListener());
	}

	// Find adjacent cells for selected BoardCell
	public void calcAdjacencies() {
		adjMatrix = new HashMap<BoardCell, Set<BoardCell>>();
		for (int row = 0; row < numRows; row++) {
			for (int col = 0; col < numColumns; col++) {
				HashSet<BoardCell> adjList = new HashSet<BoardCell>();
				BoardCell currentCell = this.getCellAt(row, col);
				if (currentCell.isDoorway()) {
					switch (currentCell.getDoorDirection()) {
						case RIGHT:
							adjList.add(this.getCellAt(row, col + 1));
							break;

						case DOWN:
							adjList.add(this.getCellAt(row + 1, col));
							break;

						case LEFT:
							adjList.add(this.getCellAt(row, col - 1));
							break;

						case UP:
							adjList.add(this.getCellAt(row - 1, col));
							break;

						default:
							break;
					}
				} else if (board[row][col].isWalkway()) {
					DoorDirection[] directions = { DoorDirection.DOWN, DoorDirection.RIGHT, DoorDirection.UP,
							DoorDirection.LEFT };
					for (int index = 0; index < directions.length; index++) {
						// set adjacent cell back to null, will leave as null if the cell would be out
						// of bounds;
						// directions refers to the direction the door would be from
						BoardCell adjCell = null;
						switch (directions[index]) {
							case DOWN:
								if (row - 1 >= 0) {
									adjCell = this.getCellAt(row - 1, col);
								}
								break;
							case RIGHT:
								if (col - 1 >= 0) {
									adjCell = this.getCellAt(row, col - 1);
								}
								break;
							case UP:
								if (row + 1 < numRows) {
									adjCell = this.getCellAt(row + 1, col);
								}
								break;
							case LEFT:
								if (col + 1 < numColumns) {
									adjCell = this.getCellAt(row, col + 1);
								}
								break;
							default:
								break;
						}
						// the check for getDoorDirection also takes care of checking if it is a doorway
						if (adjCell != null
								&& (adjCell.isWalkway() || adjCell.getDoorDirection() == directions[index])) {
							adjList.add(adjCell);
						}
					}
				}
				adjMatrix.put(currentCell, adjList);
			}
		}
	}

	// Find potential targets for selected BoardCell
	public void calcTargets(int rows, int cols, int pathLength) {
		visited = new HashSet<BoardCell>();
		targets = new HashSet<BoardCell>();
		findTargets(rows, cols, pathLength);

	}

	public void findTargets(int rows, int cols, int pathLength) {
		visited.add(board[rows][cols]);
		if (pathLength == 0 || board[rows][cols].isDoorway()) {
			if (visited.size() > 1) {
				targets.add(board[rows][cols]);
				return;
			}
		}

		for (BoardCell x : adjMatrix.get(board[rows][cols])) {
			if (!visited.contains(x)) {
				findTargets(x.getRow(), x.getColumn(), pathLength - 1);
				visited.remove(x);
			}
		}
	}

	public Map<Character, String> getLegend() {

		return legend;
	}

	public int getNumRows() {

		return numRows;
	}

	public int getNumColumns() {

		return numColumns;
	}

	public BoardCell getCellAt(int row, int cols) {
		return board[row][cols];
	}

	public Set<BoardCell> getAdjList(int rows, int cols) {

		return adjMatrix.get(board[rows][cols]);
	}

	public Set<BoardCell> getTargets() {

		return targets;
	}

	public void loadConfigFiles() throws FileNotFoundException, BadConfigFormatException {
		loadRoomConfig();
		loadBoardConfig();
		if (playerConfigFile != null) {
			loadPlayerConfig();
		}
		if (weaponConfigFile != null) {
			loadWeaponConfig();
		}
	}

	public void loadPlayerConfig() throws FileNotFoundException, BadConfigFormatException {
		if (deck == null) {
			deck = new ArrayList<Card>();
		}
		playerCards = new ArrayList<Card>();
		this.cpuPlayers = new ArrayList<ComputerPlayer>();
		FileReader reader = new FileReader(playerConfigFile);
		Scanner in = new Scanner(reader);
		while (in.hasNextLine()) {
			String name = in.nextLine();
			if (!in.hasNextLine()) {
				in.close();
				throw new BadConfigFormatException(
						"Incorrect number of lines in player config file " + playerConfigFile);
			}
			Card newPerson = new Card(name, CardType.PERSON);
			deck.add(newPerson);
			playerCards.add(newPerson);
			String color = in.nextLine();
			Color colour = convertColor(color);
			if (colour == null) {
				in.close();
				throw new BadConfigFormatException(
						"Color format for person " + name + " in player config file " + playerConfigFile);
			}
			if (!in.hasNextLine()) {
				in.close();
				throw new BadConfigFormatException(
						"Incorrect number of lines in player config file " + playerConfigFile);
			}
			String location = in.nextLine();
			String[] startingPos = location.split(" ");
			int row = 0;
			int col = 0;
			try {
				row = Integer.parseInt(startingPos[0]);
				col = Integer.parseInt(startingPos[1]);
			} catch (NumberFormatException e) {
				in.close();
				throw new BadConfigFormatException("Player " + name + " has invalid position in " + playerConfigFile);
			}
			if (this.getCellAt(row, col) == null) {
				in.close();
				throw new BadConfigFormatException("Player " + name + " is not on the board in " + playerConfigFile);
			} else if (!this.getCellAt(row, col).isWalkway()) {
				in.close();
				throw new BadConfigFormatException("Player " + name + " is not on a walkway in " + playerConfigFile);
			}
			if (humanPlayer == null) {
				HumanPlayer p = new HumanPlayer(name, colour, row, col);
				this.humanPlayer = p;
				players.add(this.humanPlayer);
			} else {
				ComputerPlayer p = new ComputerPlayer(name, colour, row, col);
				this.cpuPlayers.add(p);
				this.players.add(p);
			}
		}
		in.close();
	}

	public void loadWeaponConfig() throws FileNotFoundException, BadConfigFormatException {
		if (deck == null) {
			deck = new ArrayList<Card>();
		}
		weaponCards = new ArrayList<Card>();
		FileReader reader = new FileReader(weaponConfigFile);
		Scanner in = new Scanner(reader);
		while (in.hasNextLine()) {
			String str = in.nextLine();
			Card newWep = new Card(str, CardType.WEAPON);
			deck.add(newWep);
			weaponCards.add(newWep);
		}
		in.close();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Card> getPersons() {
		return (ArrayList<Card>) this.playerCards.clone();
	}

	@SuppressWarnings("unchecked")
	public ArrayList<Card> getWeapons() {
		return (ArrayList<Card>) this.weaponCards.clone();
	}

	public void selectAnswer() {
		Card person = null;
		Card weapon = null;
		Card room = null;
		for (Card c : deck) {
			switch (c.type) {
				case PERSON:
					if (person == null) {
						person = c;
					}
					break;
				case ROOM:
					if (room == null) {
						room = c;
					}
					break;
				case WEAPON:
					if (weapon == null) {
						weapon = c;
					}
					break;
				default:
					break;
			}
		}
		this.solution = new Solution(person, weapon, room);
	}

	public Card handleSuggestion(Solution suggestion, Player accuser) {
		for (Player p : players) {
			if (suggestion.person.name.equals(p.getPlayerName())) {
				p.moveTo(this.getCellAt(accuser.getRow(), accuser.getColumn()));
			}
		}
		this.suggestion = suggestion;
		int index = players.indexOf(accuser);
		if (index < 0) {
			// not a valid accuser, might want to throw an error
			this.response = null;
			this.wasDisproved = false;
			return null;
		}
		// loop goes through the players in order starting with the player after the
		// accuser, does not ask the accuser.
		for (int disproverIndex = index + 1; disproverIndex != index; ++disproverIndex) {
			if (disproverIndex >= players.size()) {
				disproverIndex -= players.size();
				if (disproverIndex == index) {
					break;
				}
			}
			Card result = players.get(disproverIndex).disproveSuggestion(suggestion);
			if (result != null) {
				this.response = result;
				this.wasDisproved = true;
				return result;
			}
		}
		this.response = null;
		this.wasDisproved = false;

		ControlGUI.getInstance().repaint();
		return null;
	}

	public boolean checkAccusation(Solution accusation) {
		if (solution == null) {
			return false;
		}
		if (this.solution.equals(accusation)) {
			if (this.getCurrentPlayer() instanceof ComputerPlayer) {
				JOptionPane.showMessageDialog(ClueGame.getInstance(), this.getCurrentPlayer().getPlayerName()
						+ " wins!\nThe crime was committed by " + getSuggestion() + ".");
				ClueGame.getInstance().dispose();
			}
			return true;
		}
		return false;
	}

	public ArrayList<Card> getDeck() {
		return this.deck;
	}

	public void shuffleDeck() {
		Collections.shuffle(deck);
	}

	/**
	 * This method also creates the solution
	 */
	public void dealDeck() {
		shuffleDeck();
		selectAnswer();
		int currentPlayer = 0;
		for (Card c : deck) {
			boolean willDeal = true;
			switch (c.type) {
				case PERSON:
					if (c.equals(solution.person)) {
						willDeal = false;
					}
					break;
				case ROOM:
					if (c.equals(solution.room)) {
						willDeal = false;
					}
					break;
				case WEAPON:
					if (c.equals(solution.weapon)) {
						willDeal = false;
					}
					break;
				default:
					break;
			}
			if (willDeal) {
				// deals card to the human last, increments through cpuPlayers
				if (currentPlayer == cpuPlayers.size()) {
					humanPlayer.giveCard(c);
					currentPlayer = 0;
				} else {
					cpuPlayers.get(currentPlayer).giveCard(c);
					currentPlayer++;
				}
			}
		}
	}

	public HumanPlayer getHumanPlayer() {
		return this.humanPlayer;
	}

	public ArrayList<ComputerPlayer> getComputerPlayers() {
		return this.cpuPlayers;
	}

	public ArrayList<Player> getPlayers() {
		return this.players;
	}

	public Solution getTheAnswer() {
		return this.solution;
	}

	public static Color convertColor(String strColor) {
		Color color;
		try {
			color = new Color(Integer.valueOf(strColor.substring(0, 2), 16),
					Integer.valueOf(strColor.substring(2, 4), 16), Integer.valueOf(strColor.substring(4, 6), 16));
		} catch (Exception e) {
			color = null; // Not defined
		}
		return color;
	}

	@Override
	public void paintComponent(Graphics g) {
		// first draw the backdrop
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, numColumns * BoardCell.CELL_WIDTH, numRows * BoardCell.CELL_HEIGHT);
		g.setColor(Color.CYAN);
		g.fillRect(0, 0, numColumns * BoardCell.CELL_WIDTH, numRows * BoardCell.CELL_HEIGHT);
		for (int row = 0; row < numRows; ++row) {
			for (int col = 0; col < numColumns; ++col) {
				// if the player has not moved yet, highlight all the members of targets
				// the second argument is true if the square should be targeted
				this.getCellAt(row, col).draw(g,
						!this.humanHasMoved && this.targets.contains(this.getCellAt(row, col)));

			}
		}

		for (Player p : this.players) {
			p.draw(g);
		}

		// then iterate through the entries of legend
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(numColumns * BoardCell.CELL_WIDTH + 1, numRows * BoardCell.CELL_HEIGHT + 1);
	}

	public Player getCurrentPlayer() {
		return this.players.get(currentPlayerIndex);
	}

	public boolean doTurn() {
		// generate a new roll between 6 and 1 inclusive
		this.roll = rand.nextInt(6) + 1;

		Player current = this.getCurrentPlayer();
		// update targets
		this.calcTargets(current.getRow(), current.getColumn(), roll);
		// repaint control gui

		// human
		if (currentPlayerIndex == 0) {
			humanHasMoved = false;
			this.suggestion = null;
			this.response = null;
			ControlGUI.getInstance().repaint();
			this.repaint();
			// after this we must wait for the human to make a move, and that would be
			// handled in the human's suggestion part
		} else {

			this.repaint();
			// computer turn
			Player tempP = this.getCurrentPlayer();
			if (!(tempP instanceof ComputerPlayer)) {
				System.out.println("ERROR occured in board class in doTurn method");
			}
			ComputerPlayer currPlayer = (ComputerPlayer) this.getCurrentPlayer();
			Solution accusation = currPlayer.makeAccusation();
			if (accusation != null) {
				if (this.checkAccusation(accusation)) {

				}

			}
			BoardCell toMove = currPlayer.pickLocation(targets);
			currPlayer.moveTo(toMove);

			if (toMove.isDoorway()) {
				this.suggestion = currPlayer.createSuggestion();
				Card result = this.handleSuggestion(suggestion, currPlayer);
				if (result != null) {
					// show all players the cards.
					for (Player p : this.players) {
						p.showCard(result);
					}
					wasDisproved = true;
				}

				else {
					// let the human player know that no card to disprove was returned
					wasDisproved = false;
				}
				this.getParent().repaint();
			} else {
				this.suggestion = null;
				this.response = null;
			}

		}
		ControlGUI.getInstance().repaint();
		return false;
	}

	/**
	 * 
	 * @return True if the method call was successful
	 */
	public boolean nextPlayer() {
		if (this.currentPlayerIndex == 0) {
			if (!humanHasMoved) {
				return false;
			}
		}
		// iterate current player
		this.currentPlayerIndex++;
		if (this.currentPlayerIndex >= this.players.size()) {
			this.currentPlayerIndex = 0;
		}
		// doTurn should work for both human and computer player
		this.doTurn();
		this.getParent().repaint();
		return true;
	}

	private class BoardListener implements MouseListener {
		// Empty definitions for unused event methods
		public void mouseEntered(MouseEvent event) {
		}

		public void mouseExited(MouseEvent event) {
		}

		public void mousePressed(MouseEvent event) {
		}

		public void mouseReleased(MouseEvent event) {
		}

		public void mouseClicked(MouseEvent event) {
			int col = event.getX() / BoardCell.CELL_WIDTH;
			int row = event.getY() / BoardCell.CELL_HEIGHT;
			// human turn
			System.out.println(row + " " + col);
			if (currentPlayerIndex == 0) {
				if (humanHasMoved) {
					// player has already moved
					JOptionPane.showMessageDialog(ClueGame.getInstance(), "Player has already moved this turn");
				} else {
					BoardCell target = getCellAt(row, col);
					if (targets.contains(target)) {
						boolean playerCanSuggest = target.isDoorway()
								&& target.getInitial() != humanPlayer.getLastRoom();
						players.get(currentPlayerIndex).moveTo(target);
						humanHasMoved = true;
						if (playerCanSuggest) {
							// make the dialog box
							SuggestionBoxPanel suggestionPanel = new SuggestionBoxPanel(false);
							suggestionPanel.setVisible(true);
						} else {
							suggestion = null;
						}
					} else {
						// not a valid target square, let them know
						JOptionPane.showMessageDialog(ClueGame.getInstance(), "Player cannot move to this square");
					}
				}
			}

			theInstance.repaint();
			theInstance.getParent().repaint();

		}
	}

	public int getRoll() {
		return this.roll;
	}

	public Card getResponse() {
		return this.response;
	}

	public Solution getSuggestion() {
		return this.suggestion;
	}

	public boolean wasLastSuggestionDisproved() {
		return wasDisproved;
	}

	public boolean humanHasMoved = false;

}
