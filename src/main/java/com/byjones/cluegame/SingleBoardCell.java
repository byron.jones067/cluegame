package com.byjones.cluegame;

public class SingleBoardCell {

	private int column, row = 0;

	public int getColumn() {
		return column;
	}

	public int getRow() {
		return row;
	}

	public SingleBoardCell(int col, int row) {
		super();
		this.column = col;
		this.row = row;
	}
}
