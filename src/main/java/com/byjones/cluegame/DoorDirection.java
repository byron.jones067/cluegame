package com.byjones.cluegame;

public enum DoorDirection {
	UP(), DOWN(), LEFT(), RIGHT(), NONE();
}
