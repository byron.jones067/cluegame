package com.byjones.cluegame;

public class Card {
	public final String name;
	public final CardType type;

	public Card(String name, CardType type) {
		this.type = type;
		this.name = name;
	}

	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		}
		if (!(o instanceof Card)) {
			return false;
		}
		Card c = (Card) o;
		return (this.name.equals(c.name)) && this.type == c.type;
	}

	@Override
	public String toString() {
		return this.name;
	}
}
