package com.byjones.cluegame;

import java.awt.BorderLayout;
//import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//import javax.swing.Box;
//import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class ClueGame extends JFrame {

	private static final long serialVersionUID = 1L;
	private DetectiveNotesPanel detectiveNotes = null;
	private ControlGUI control;
	private JPanel playerHand;
	private static ClueGame theInstance = new ClueGame();

	private ClueGame() {
		// setSize(new Dimension(800, 350));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		Board board = Board.getInstance();
		board.setConfigFiles("MBSR_ClueLayout.csv", "MBSR_ClueLegend.txt", "MBSR_test_players.txt",
				"MBSR_test_weapons.txt");
		board.initialize();

		detectiveNotes = new DetectiveNotesPanel();
		detectiveNotes.setVisible(false);
		this.setLayout(new BorderLayout(2, 0));

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		menuBar.add(createFileMenu());

		control = this.createControlGUI();
		playerHand = createHumanPlayerHand();

		add(board, BorderLayout.CENTER);
		add(playerHand, BorderLayout.EAST);
		add(control, BorderLayout.SOUTH);
		pack();
	}

	public static ClueGame getInstance() {
		return theInstance;
	}

	private ControlGUI createControlGUI() {
		return ControlGUI.getInstance();
	}

	private JPanel createHumanPlayerHand() {
		JPanel playerHand = new JPanel();
		playerHand.setLayout(new GridLayout(0, 1));
		JPanel peopleCards = new JPanel();
		JPanel roomCards = new JPanel();
		JPanel weaponsCards = new JPanel();
		peopleCards.setLayout(new GridLayout(0, 1));
		roomCards.setLayout(new GridLayout(0, 1));
		weaponsCards.setLayout(new GridLayout(0, 1));
		peopleCards.setBorder(new TitledBorder("Players"));
		roomCards.setBorder(new TitledBorder("Rooms"));
		weaponsCards.setBorder(new TitledBorder("Weapons"));
		for (Card c : Board.getInstance().getHumanPlayer().getMyCards()) {
			JTextField newCard = new JTextField(c.name);
			newCard.setEditable(false);
			switch (c.type) {
				case PERSON:
					peopleCards.add(newCard);
					break;
				case ROOM:
					roomCards.add(newCard);
					break;
				case WEAPON:
					weaponsCards.add(newCard);
					break;
				default:
					break;
			}
		}
		playerHand.add(peopleCards);
		playerHand.add(roomCards);
		playerHand.add(weaponsCards);
		playerHand.setBorder(new TitledBorder("My Cards"));
		return playerHand;
	}

	private JMenu createFileMenu() {
		JMenu menu = new JMenu("File");
		menu.add(createDetectiveNotesPanel());
		menu.add(createFileExitItem());

		return menu;
	}

	private JMenuItem createFileExitItem() {
		JMenuItem item = new JMenuItem("Exit");
		class MenuItemListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		}
		item.addActionListener(new MenuItemListener());

		return item;
	}

	private JMenuItem createDetectiveNotesPanel() {
		JMenuItem item = new JMenuItem("Toggle Detective Notes");
		class MenuItemListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				if (detectiveNotes != null) {
					detectiveNotes.setVisible(!detectiveNotes.isVisible());
				}
			}
		}
		item.addActionListener(new MenuItemListener());

		return item;
	}

	public static void main(String[] args) {
		ClueGame clueGame = getInstance();
		clueGame.setVisible(true);
		JOptionPane.showMessageDialog(clueGame,
				"You are " + Board.getInstance().getHumanPlayer().getPlayerName() + ", press Next Player to begin play",
				"Welcome to Clue?", JOptionPane.INFORMATION_MESSAGE);
		// start the first turn of the game
		Board.getInstance().doTurn();
	}

}
