package com.byjones.cluegame;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SuggestionBoxPanel extends JDialog {

	private static final long serialVersionUID = 1L;
	private BoardCell playerLoc;
	private String roomString;
	private JComboBox<String> room;
	private JComboBox<String> people;
	private JComboBox<String> weapons;
	private Card selectPerson, selectWeapon, selectRoom;
	private boolean isAccusation = false;

	public SuggestionBoxPanel(boolean isAccusation) {
		super(ClueGame.getInstance(), isAccusation ? "Make an Accusation" : "Make a suggestion",
				Dialog.ModalityType.APPLICATION_MODAL);
		setSize(new Dimension(400, 200));

		// setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		this.playerLoc = Board.getInstance().getCellAt(Board.getInstance().getCurrentPlayer().getRow(),
				Board.getInstance().getHumanPlayer().getColumn());
		this.roomString = Board.getInstance().getLegend().get(playerLoc.getInitial());
		this.isAccusation = isAccusation;
		JPanel jp = createAccusationPanel();
		add(jp);

	}

	public JPanel createAccusationPanel() {
		JPanel result = new JPanel();
		result.setLayout(new GridLayout(0, 2));

		JLabel jlRoom = new JLabel();
		JLabel jlPerson = new JLabel();
		JLabel jlWeapon = new JLabel();
		people = new JComboBox<String>();
		weapons = new JComboBox<String>();
		room = new JComboBox<String>();
		JButton submitButton = new JButton();
		submitButton.setText("Submit");
		submitButton.addActionListener(new submitListener());
		JButton cancelButton = new JButton();
		cancelButton.setText("Cancel");
		cancelButton.addActionListener(new cancelListener());

		for (Card c : Board.getInstance().getDeck()) {
			switch (c.type) {
				case PERSON:
					people.addItem(c.name);
					break;
				case ROOM:
					room.addItem(c.name);
					break;
				case WEAPON:
					weapons.addItem(c.name);
					break;
				default:
					break;

			}
		}
		jlRoom.setText("Your room");
		result.add(jlRoom);
		if (this.isAccusation) {
			result.add(room);
		} else {
			JTextField room = new JTextField(this.roomString);
			room.setEditable(false);
			result.add(room);
		}
		jlPerson.setText("Person");
		result.add(jlPerson);
		result.add(people);
		jlWeapon.setText("Weapon");
		result.add(jlWeapon);
		result.add(weapons);
		result.add(submitButton);
		result.add(cancelButton);

		return result;
	}

	class submitListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			selectRoom = new Card(isAccusation ? room.getSelectedItem() + "" : roomString, CardType.ROOM);
			selectWeapon = new Card(weapons.getSelectedItem() + "", CardType.WEAPON);
			selectPerson = new Card(people.getSelectedItem() + "", CardType.PERSON);
			if (isAccusation) {
				if (Board.getInstance().checkAccusation(getSuggestion())) {
					// the game ends, player wins
					JOptionPane.showMessageDialog(ClueGame.getInstance(),
							"You win!\nThe crime was committed by " + getSuggestion() + ".");
					dispose();
					ClueGame.getInstance().dispose();
				} else {
					JOptionPane.showMessageDialog(ClueGame.getInstance(), "That is incorrect.");
				}
			} else {
				Board.getInstance().handleSuggestion(getSuggestion(), Board.getInstance().getCurrentPlayer());
				ControlGUI.getInstance().repaint();
				Board.getInstance().humanHasMoved = true;
				dispose();
			}
		}
	}

	class cancelListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			dispose();
		}
	}

	public Solution getSuggestion() {
		return new Solution(this.selectPerson, this.selectWeapon, this.selectRoom);
	}
}
