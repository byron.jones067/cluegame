package com.byjones.cluegame;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class DetectiveNotesPanel extends JDialog {

	private static final long serialVersionUID = 1L;
	private ArrayList<Card> deck;
	private ArrayList<Card> people;
	private ArrayList<Card> weapons;

	public DetectiveNotesPanel() {
		setSize(new Dimension(800, 350));

		setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
		this.deck = Board.getInstance().getDeck();
		this.people = Board.getInstance().getPersons();
		this.weapons = Board.getInstance().getWeapons();
		this.setLayout(new GridLayout(3, 1));
		JPanel jp = createPeoplePanel();
		add(jp);
		jp = createRoomsPanel();
		add(jp);
		jp = createWeaponsPanel();
		add(jp);
	}

	public JPanel createPeoplePanel() {
		JPanel result = new JPanel();
		result.setLayout(new GridLayout(1, 2));
		JPanel people = new JPanel();
		people.setLayout(new GridLayout(0, 2));
		JCheckBox checkBox;
		JComboBox<String> peopleGuess = new JComboBox<String>();
		peopleGuess.addItem("Unsure");
		for (Card c : this.people) {
			checkBox = new JCheckBox(c.name);
			people.add(checkBox);
			peopleGuess.addItem(c.name);
		}
		people.setBorder(new TitledBorder("People"));
		peopleGuess.setBorder(new TitledBorder("Person Guess"));
		result.add(people);
		result.add(peopleGuess);
		return result;
	}

	public JPanel createRoomsPanel() {
		JPanel result = new JPanel();
		result.setLayout(new GridLayout(1, 2));
		JPanel rooms = new JPanel();
		rooms.setLayout(new GridLayout(0, 2));
		JCheckBox checkBox;
		JComboBox<String> roomGuess = new JComboBox<String>();
		roomGuess.addItem("Unsure");
		for (Card c : this.deck) {
			if (c.type == CardType.ROOM) {
				checkBox = new JCheckBox(c.name);
				rooms.add(checkBox);
				roomGuess.addItem(c.name);
			}
		}
		rooms.setBorder(new TitledBorder("Rooms"));
		roomGuess.setBorder(new TitledBorder("Room Guess"));
		result.add(rooms);
		result.add(roomGuess);
		return result;
	}

	public JPanel createWeaponsPanel() {
		JPanel result = new JPanel();
		result.setLayout(new GridLayout(1, 2));
		JPanel weapons = new JPanel();
		weapons.setLayout(new GridLayout(0, 2));
		JCheckBox checkBox;
		JComboBox<String> weaponsGuess = new JComboBox<String>();
		weaponsGuess.addItem("Unsure");
		for (Card c : this.weapons) {
			checkBox = new JCheckBox(c.name);
			weapons.add(checkBox);
			weaponsGuess.addItem(c.name);
		}
		weapons.setBorder(new TitledBorder("Weapons"));
		weaponsGuess.setBorder(new TitledBorder("Weapon Guess"));
		result.add(weapons);
		result.add(weaponsGuess);
		return result;
	}
}
