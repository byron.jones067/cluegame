package com.byjones.cluegame;

public class Solution {
	public final Card person;
	public final Card weapon;
	public final Card room;

	public Solution(Card person, Card weapon, Card room) {
		this.person = person;
		this.weapon = weapon;
		this.room = room;
	}

	@Override
	public boolean equals(Object o) {
		// general object equals method
		if (o == null) {
			return false;
		}
		if (!(o instanceof Solution)) {
			return false;
		}
		Solution s = (Solution) o;
		return this.person.equals(s.person) && this.room.equals(s.room) && this.weapon.equals(s.weapon);
	}

	@Override
	public String toString() {
		return person + " with the " + weapon + " in the " + room;
	}
}
