package com.byjones.cluegame;

import java.awt.Color;
import java.util.Set;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;

public class ComputerPlayer extends Player {
	public ComputerPlayer(String name, Color colour, int row, int col) {
		super(name, colour, row, col);
	}

	@Override
	public Card disproveSuggestion(Solution suggestion) {
		ArrayList<Card> matches = new ArrayList<Card>();
		for (Card card : this.getMyCards()) {
			if (suggestion.person.equals(card)) {
				matches.add(card);
			} else if (suggestion.room.equals(card)) {
				matches.add(card);
			} else if (suggestion.weapon.equals(card)) {
				matches.add(card);
			}
		}
		Collections.shuffle(matches);
		if (matches.isEmpty()) {
			return null;
		}
		// else not needed
		// shuffled matches to get the 0 to be random
		return matches.get(0);
	}

	public BoardCell pickLocation(Set<BoardCell> targets) {
		boolean hasNewRoom = false;
		Set<BoardCell> possibilities = new HashSet<BoardCell>();
		for (BoardCell cell : targets) {
			if (cell.isRoom()) {
				if (cell.getInitial() != this.getLastRoom()) {
					if (!hasNewRoom) {

						possibilities.clear();
						hasNewRoom = true;
					}
					possibilities.add(cell);
				} else {
					if (!hasNewRoom) {
						possibilities.add(cell);
					}
				}
			} else {
				if (!hasNewRoom) {
					possibilities.add(cell);
				}
			}
		}
		int selected = (int) Math.floor(Math.random() * possibilities.size());
		BoardCell selectedCell = null;
		int index = 0;
		// Index represents the position in the iterator that the current BoardCell
		// represents
		for (BoardCell cell : possibilities) {
			if (selected == index) {
				selectedCell = cell;
			}
			++index;
		}
		return selectedCell;
	}

	public Solution makeAccusation() {
		if (!Board.getInstance().wasLastSuggestionDisproved()) {
			return Board.getInstance().getSuggestion();
		}
		return null;
	}

	// should only be called after a room was gotten to.
	public Solution createSuggestion() {
		Board board = Board.getInstance();
		@SuppressWarnings("unchecked")
		ArrayList<Card> persons = (ArrayList<Card>) board.getPersons().clone();
		@SuppressWarnings("unchecked")
		ArrayList<Card> weapons = (ArrayList<Card>) board.getWeapons().clone();
		String roomName = board.getLegend().get(board.getCellAt(this.getRow(), this.getColumn()).getInitial());
		Card room = new Card(roomName, CardType.ROOM);
		for (Card c : this.getSeenCards()) {
			persons.remove(c);
			weapons.remove(c);
		}
		Card person = persons.get((new Random()).nextInt(persons.size()));
		Card weapon = weapons.get((new Random()).nextInt(weapons.size()));
		return new Solution(person, weapon, room);
	}

	@Override
	public void moveTo(BoardCell boardCell) {
		super.moveTo(boardCell);

	}
}
