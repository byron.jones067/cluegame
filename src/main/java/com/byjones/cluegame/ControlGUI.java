package com.byjones.cluegame;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class ControlGUI extends JPanel {

	private static final long serialVersionUID = 1L;
	private JTextField playerName;
	private JTextField diceRoll;
	private JTextField guess;
	private JTextField response;
	private JButton nextPlayer;
	private JButton accuse;

	public static ControlGUI getInstance() {
		return theInstance;
	}

	private static ControlGUI theInstance = new ControlGUI();

	private ControlGUI() {
		this.setLayout(new GridLayout(3, 1));
		this.add(this.createButtons());
		this.add(this.createPlayerAndRoll());
		this.add(this.createGuessInfo());
		this.setMaximumSize(new Dimension(1000, 200));
	}

	private JPanel createGuessInfo() {
		JPanel result = new JPanel();
		guess = new JTextField("A guess consisting of a weapon, player, and room", 50);
		guess.setEditable(false);
		guess.setBorder(new TitledBorder(new EtchedBorder(), "Guess"));
		guess.setToolTipText("The Guess made, if one was made");
		result.add(guess);
		response = new JTextField("Disproving card", 20);
		response.setEditable(false);
		response.setBorder(new TitledBorder(new EtchedBorder(), "Guess Response"));
		result.add(response);
		return result;
	}

	private JPanel createButtons() {
		JPanel result = new JPanel();
		nextPlayer = new JButton("Next player");
		nextPlayer.setToolTipText("Goes to the next player's turn");
		accuse = new JButton("Make an accusation");
		accuse.setToolTipText("Allows the making of an accusation");
		result.add(nextPlayer);
		nextPlayer.addActionListener(new NextPlayerListener());
		accuse.addActionListener(new accuseListener());
		result.add(accuse);
		return result;
	}

	class NextPlayerListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {

			if (!Board.getInstance().nextPlayer()) {
				JOptionPane.showMessageDialog(ClueGame.getInstance(), "Player needs to finish their turn");
			}
		}
	}

	private JPanel createPlayerAndRoll() {
		JPanel result = new JPanel();
		JTextField name = new JTextField(Board.getInstance().getCurrentPlayer().getPlayerName(), 20);
		name.setEditable(false);
		name.setBorder(new TitledBorder(new EtchedBorder(), "Current Player"));
		name.setToolTipText("The name of the current player");
		result.add(name);
		this.playerName = name;
		diceRoll = new JTextField("-", 3);
		diceRoll.setEditable(false);
		diceRoll.setBorder(new TitledBorder(new EtchedBorder(), "Roll"));
		result.add(diceRoll);
		return result;
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(Board.getInstance().getNumColumns() * BoardCell.CELL_WIDTH + 1, 150);
	}

	protected void updatePlayer() {
		this.playerName.setText(Board.getInstance().getCurrentPlayer().getPlayerName());
	}

	private void updateDiceRoll() {
		this.diceRoll.setText(Board.getInstance().getRoll() + "");
	}

	public void updateGuess(Solution s) {
		this.guess.setText(s != null ? s + "" : "");
	}

	public void updateResponse(Card c) {
		if (Board.getInstance().getSuggestion() != null) {
			this.response.setText(c != null ? c + "" : "No new clues");
		} else {
			this.response.setText("");
		}

	}

	/*
	 * public static void main(String[] args) { JFrame frame = new JFrame();
	 * frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); frame.setTitle("GUI");
	 * frame.setSize(750, 200); ControlGUI gui = new ControlGUI(); frame.add(gui,
	 * BorderLayout.CENTER); frame.setVisible(true); }
	 */
	@Override
	public void repaint() {
		if (this.playerName != null) {
			this.updatePlayer();
		}
		if (this.diceRoll != null) {
			this.updateDiceRoll();
		}
		if (this.response != null) {

			this.updateResponse(Board.getInstance().getResponse());
		}
		if (this.guess != null) {
			this.updateGuess(Board.getInstance().getSuggestion());
		}
		super.repaint();
	}

	class accuseListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (Board.getInstance().getCurrentPlayer() instanceof HumanPlayer) {
				if (!Board.getInstance().humanHasMoved) {
					SuggestionBoxPanel suggestionPanel = new SuggestionBoxPanel(true);
					suggestionPanel.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(ClueGame.getInstance(), "Your turn has already finished");
				}
			} else {
				JOptionPane.showMessageDialog(ClueGame.getInstance(), "It is not your turn");
			}
		}
	}
}
