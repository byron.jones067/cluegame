package com.byjones.cluegame;

import java.awt.Color;
import java.awt.Graphics;

public class BoardCell {

	private int column, row;
	private char initial;
	private DoorDirection doorDirection;
	public static final int CELL_WIDTH = 32;
	public static final int CELL_HEIGHT = 32;
	public static final Color CELL_COLOR = new Color(255, 127, 80);// FF7F50,coral from the another color interface
	public static final Color CELL_BORDER_COLOR = Color.BLACK;
	public static final Color TARGET_COLOR = new Color(255, 255, 0);
	public static final int DOOR_HEIGHT = CELL_HEIGHT / 4;
	public static final int DOOR_WIDTH = CELL_WIDTH / 4;
	public static final Color DOOR_COLOR = Color.RED;
	private boolean isNameStart = false;

	public BoardCell(int row, int col) {
		super();
		this.column = col;
		this.row = row;
	}

	public BoardCell(int row, int col, char init) {
		super();
		this.column = col;
		this.row = row;
		this.initial = init;
		this.doorDirection = DoorDirection.NONE;
	}

	public BoardCell(int row, int col, char init, char dir) {
		super();
		this.column = col;
		this.row = row;
		this.initial = init;

		switch (dir) {
			case 'U':
				this.doorDirection = DoorDirection.UP;
				break;

			case 'D':
				this.doorDirection = DoorDirection.DOWN;
				break;

			case 'L':
				this.doorDirection = DoorDirection.LEFT;
				break;

			case 'R':
				this.doorDirection = DoorDirection.RIGHT;
				break;
			case 'N':
				this.doorDirection = DoorDirection.NONE;
				this.isNameStart = true;
				break;
			default:
				this.doorDirection = DoorDirection.NONE;
				break;
		}
	}

	public int getColumn() {
		return column;
	}

	public int getRow() {
		return row;
	}

	public char getInitial() {
		return initial;
	}

	public void setInitial(char init) {
		this.initial = init;
	}

	public boolean isWalkway() {
		if (this.initial == 'W') {
			return true;
		}
		return false;
	}

	// includes rooms marked as other
	public boolean isRoom() {
		if (this.initial != 'W') {
			return true;
		}
		return false;
	}

	public boolean isDoorway() {
		return this.doorDirection != DoorDirection.NONE;
	}

	public DoorDirection getDoorDirection() {
		return this.doorDirection;
	}

	public void draw(Graphics g) {
		this.draw(g, false);
	}

	public void draw(Graphics g, boolean targetable) {
		// top right corner
		int x = column * CELL_WIDTH;
		int y = row * CELL_HEIGHT;
		int height = CELL_HEIGHT;
		int width = CELL_WIDTH;
		boolean isDoor = this.isDoorway();
		if (isDoor) {
			switch (this.doorDirection) {
				case UP:
					// upper left corner stays the same, decrease height;
					height = DOOR_HEIGHT;
					break;
				case DOWN:
					height = DOOR_HEIGHT;
					y = y + CELL_HEIGHT - DOOR_HEIGHT;
					break;
				case LEFT:
					width = DOOR_WIDTH;
					break;
				case RIGHT:
					width = DOOR_WIDTH;
					x = x + CELL_WIDTH - DOOR_WIDTH;
					break;
				default:
					break;
			}
		} else if (!this.isWalkway()) {
			if (!this.isRoom()) {
				g.fillRect(x, y, width, height);
			} else if (this.isNameStart) {
				g.drawString(Board.getInstance().getLegend().get(this.initial), x, y);
			}
			// cell is neither a door or a walkway, so it is not drawn
			return;
		}
		g.setColor(targetable ? TARGET_COLOR : (isDoor ? DOOR_COLOR : CELL_COLOR));
		g.fillRect(x, y, width, height);
		g.setColor(CELL_BORDER_COLOR);
		g.drawRect(x, y, width, height);

	}
}
