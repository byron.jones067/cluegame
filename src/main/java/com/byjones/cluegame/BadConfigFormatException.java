package com.byjones.cluegame;

public class BadConfigFormatException extends Exception {

	private static final long serialVersionUID = 1L;

	public BadConfigFormatException() {
		super("Incorrect config format");
	}

	public BadConfigFormatException(String message) {
		super(message);
	}

}
