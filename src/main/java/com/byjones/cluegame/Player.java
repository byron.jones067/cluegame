package com.byjones.cluegame;

import java.awt.Color;
import java.awt.Graphics;
import java.util.HashSet;
import java.util.Set;

public abstract class Player {
	private String playerName;
	private int row;
	private int column;
	private Color color;
	private Set<Card> myCards;
	private Set<Card> seenCards;
	private char lastRoomChar = 0;

	public Player(String name, Color color, int row, int col) {
		this.myCards = new HashSet<Card>();
		this.seenCards = new HashSet<Card>();
		this.playerName = name;
		this.color = color;
		this.row = row;
		this.column = col;
	}

	public abstract Card disproveSuggestion(Solution suggestion);

	public Set<Card> getMyCards() {
		return myCards;
	}

	public Set<Card> getSeenCards() {
		return seenCards;
	}

	public String getPlayerName() {
		return this.playerName;
	}

	public Color getColor() {
		return this.color;
	}

	public int getRow() {
		return this.row;
	}

	public int getColumn() {
		return this.column;
	}

	public void giveCard(Card card) {
		this.myCards.add(card);
		this.seenCards.add(card);
	}

	public void moveTo(BoardCell cell) {
		this.row = cell.getRow();
		this.column = cell.getColumn();
		if (cell.isRoom()) {
			this.lastRoomChar = cell.getInitial();
		}
	}

	public void showCard(Card shown) {
		seenCards.add(shown);
	}

	public void draw(Graphics g) {
		int playerX = BoardCell.CELL_WIDTH * this.column;
		int playerY = BoardCell.CELL_HEIGHT * this.row;

		g.setColor(this.color);
		g.fillOval(playerX, playerY, BoardCell.CELL_WIDTH, BoardCell.CELL_HEIGHT);

		g.setColor(PLAYER_BORDER);
		g.drawOval(playerX, playerY, BoardCell.CELL_WIDTH, BoardCell.CELL_HEIGHT);
	}

	public char getLastRoom() {
		return lastRoomChar;
	}

	public static final Color PLAYER_BORDER = Color.BLACK;
}
